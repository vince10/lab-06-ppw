# Lab-06 PPW

## [Website](https://story-6-landing.herokuapp.com/)
###  

Vincentius Adi Kurnianto
1706979480
PPW E

# Pipeline

[![pipeline status](https://gitlab.com/vince10/lab-06-ppw/badges/master/pipeline.svg)](https://gitlab.com/vince10/lab-06-ppw/commits/master)

# Code Coverage 

[![coverage report](https://gitlab.com/vince10/lab-06-ppw/badges/master/coverage.svg)](https://gitlab.com/vince10/lab-06-ppw/commits/master)