from django.contrib import admin
from .models import Status,Subscriber

# Register your models here.
admin.site.register(Status)
admin.site.register(Subscriber)
