from django import forms
from .models import Subscriber



class Status_Form(forms.Form):
    error_messages = {
        'required': 'Please type',
    }
    status_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder': 'status',
        'id':'statuss',
        
    }
    # place_attrs = {
    #     'type': 'text',
    #     'class': 'todo-form-input',
    #     'placeholder':'place'
    # }
    # category_attrs = {
    #     'type': 'text',
    #     'class': 'todo-form-input',
    #     'placeholder':'category'
    # }
    # date_attrs={
    #     'type':'date'
        
    # }

    # category_choices=(
    #     ('indoor','indoor'),
    #     ('outdoor','outdoor'),
    #     ('a bit of both','a bit of both'),
    # )
    
    status = forms.CharField(label='Status', required=True, max_length=67, widget=forms.TextInput(attrs=status_attrs))
    # place = forms.CharField(label='Place', required=True, max_length=27, widget=forms.TextInput(attrs=place_attrs))
    # #category = forms.CharField(label='Category', required=True,max_length=27,choices=category_choices)
    # category = forms.ChoiceField(label='Category', required=True,choices=category_choices)
    # #date = forms.CharField(label='Date', required=True, max_length=27, widget=forms.DateInput(attrs=date_attrs))
    # date = forms.DateField(label='Date',required=True,widget=forms.DateInput(attrs=date_attrs))
    # time = forms.TimeField(label='Time',required=True,widget=forms.TimeInput(attrs={'type':'time'}))
class Login_Form(forms.Form):
    username = forms.CharField(label='username', required=True, max_length=67, widget=forms.TextInput(attrs={
        'placeholder':'username'
    }))
    password = forms.CharField(label='password', required=True, max_length=67, widget=forms.PasswordInput(attrs={
        'placeholder':'password'
    }))



class Subscriber_Form(forms.ModelForm):
 
    class Meta:
        model = Subscriber
        fields =[
            'name',
            'email',
            'password',
        ]
        widgets = {
            'name' : forms.TextInput(attrs={'id':'name'}),
            'email' : forms.EmailInput(attrs={'id':'email'}),
            'password': forms.PasswordInput(attrs={'id':'password'})
            
        }
        

    
