from django.db import models
from django.utils import timezone

class Status(models.Model):
    dates = models.DateTimeField(default=timezone.now())
    status = models.TextField(max_length=300)

# class Diary(models.Model):
#     tanggal =  models.DateTimeField(default=timezone.now())
#     catatan = models.TextField(max_length=300)
class Subscriber(models.Model):
    name =  models.CharField(max_length=300)
    email = models.EmailField(max_length=100, null=True, blank=True)
    password =  models.CharField(max_length=100)

    def __str__(self):
        return self.name+"          "+self.email+"          "+self.password
