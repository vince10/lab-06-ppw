from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index,profile,email_checking,subscribe
from .models import Status,Subscriber
from .forms import Status_Form,Subscriber_Form
from django.utils import timezone
import time
import json
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
# class Story6FunctionalTest(TestCase):
    
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable') 
#         chrome_options.add_argument('--no-sandbox') 
#         chrome_options.add_argument('--headless') 
#         chrome_options.add_argument('disable-gpu') 
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         #self.browser.implicitly_wait(25)
#         super(Story6FunctionalTest, self).setUp()

#     def tearDown(self):
#         self.browser.quit() 
#         super(Story6FunctionalTest, self).tearDown()
    
#     def test_input_status(self):
#         browser = self.browser
#         time.sleep(3)
#         browser.get('http://127.0.0.1:8000/') #    http://localhost:8000/
#         time.sleep(3)
#         status = browser.find_element_by_id("statuss")
#         string = 'Coba Coba'
#         status.send_keys(string)
#         time.sleep(3)
#         submit =  browser.find_element_by_id('submit')
#         submit.send_keys(Keys.RETURN)
#         time.sleep(3)
#         self.assertIn(string,browser.page_source)
#     def test_background_color_greeting(self):
#         browser = self.browser
#         browser.get('http://127.0.0.1:8000/') #    http://localhost:8000/
#         time.sleep(3)
#         color = browser.find_element_by_id('backgreeting').value_of_css_property('background-color')
#         self.assertEqual('rgba(224, 255, 255, 1)',color)

#     def test_font_coler_used_in_greeting(self):
#         browser = self.browser
#         browser.get('http://127.0.0.1:8000/') #    http://localhost:8000/
#         time.sleep(3)
#         font_color = browser.find_element_by_tag_name('h4').value_of_css_property('color')
#         self.assertIn('rgba(33, 37, 41, 1)',font_color)
      
#     def test_name_of_title_in_page(self):
#         browser = self.browser
#         browser.get('http://127.0.0.1:8000/') #    http://localhost:8000/
#         time.sleep(3)
#         self.assertIn("Homepage",browser.title)
        
#     def test_Hello_There(self):
#         browser = self.browser
#         browser.get('http://127.0.0.1:8000/') #    http://localhost:8000/
#         time.sleep(3)
#         hello_there = browser.find_element_by_tag_name('h4').text
#         self.assertIn("Hello There !",hello_there)

        
#     def test_button_mechanism_worked(self):
#         browser = self.browser
#         browser.get('http://127.0.0.1:8000/') #    http://localhost:8000/
#         time.sleep(3)
#         button = browser.find_element_by_id('info')
#         button.send_keys(Keys.RETURN)
#         time.sleep(3)
#         background_img = browser.find_element_by_id("myDiv").value_of_css_property('background-image')
#         font_color = browser.find_element_by_tag_name('h3').value_of_css_property('color')
#         self.assertEqual(background_img,'url("http://127.0.0.1:8000/static/haha.jpg")')
#         self.assertEqual(font_color,'rgba(245, 222, 179, 1)')
      
    
   
        
class Story10UnitTest(TestCase):
    def test_url_is_exists(self):
        response_2 = Client().get('/emailcheck/')
        response_3 = Client().get('/subscribe/')
        self.assertEqual(response_2.status_code,200)
        self.assertNotEqual(response_3.status_code,200)
    def test_function_to_url(self):
        found_2 = resolve('/emailcheck/')
        found_3 = resolve('/subscribe/')
        self.assertEqual(found_2.func,email_checking)
        self.assertEqual(found_3.func,subscribe)
    def test_subscriber_form(self):
        data ={
            'name':'Setiaki',
            'email':'abc@gmail.com',
            'password':'password'
        }
        form = Subscriber_Form(data=data)
        self.assertTrue(form.is_valid())
    def test_model_can_create_subscriber(self):
        subscriber = Subscriber.objects.create(name='Setiaki',email='abc@gmail.com',password='password')
        counting_all_subscriber = Subscriber.objects.all().count()
        self.assertEqual(counting_all_subscriber,1)
    def test_email_check(self):
        fail_message =  "This email is already registered, Please choose another email"
        success_message = "You can use this email!"
        subscriber_1 = Subscriber.objects.create(name='Setiaki',email='abcd@gmail.com',password='password')
        response = Client().post('/emailcheck/',{'email':'abc@gmail.com'})
        self.assertEqual(response.json()['message'],success_message)
        self.assertEqual(response.json()['status_code'],200)
        response = Client().post('/emailcheck/',{'email':'abcd@gmail.com'})
        print(response.json())
        self.assertEqual(response.json()['message'],fail_message)
        self.assertEqual(response.json()['status_code'],400)
    # def test_create_subscriber(self):
    #     data = {'name':'Setiaki','email':'ganteng@gmail.com','password':'password'}
    #     response = Client().post('/subscribe/',data=data)
    #     success_string = "Subscriber created"
    #     self.assertEqual(response.json()['message'],success_string)
     


class Story6UnitTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_using_landing_page_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'html_baru.html')

    def test_forms(self):
        form_data = {'status': 'smental_health'}
        form = Status_Form(data=form_data)
        self.assertTrue(form.is_valid())


    def test_model_can_create_status(self):
        status = Status.objects.create(status='status')

        counting_all_status = Status.objects.all().count()
        self.assertEqual(counting_all_status, 1)

    def test_status_validation_for_blank_items(self):
            form = Status_Form(data={'status': '','dates':timezone.now()})
            self.assertFalse(form.is_valid())
            self.assertEqual(
                form.errors['status'],
                ['This field is required.']
            )

    def test_post_success_and_render_the_result(self):
        test = 'Feel freedooo'
        response_post = Client().post('/save_status/', {'status': test, 'dates':timezone.now()})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)



    def test_post_error_exceeding_300(self):
        test = '00'*300
        response_post = Client().post('/save_status/', {'stattus': test, 'dates':timezone.now()})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_url_profile_exists(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)
    
    def test_profile_using_proflie_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)

    def test_using_landing_page_templates(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'homepage.html')










    

    






