from django.urls import re_path
from django.urls import path
#from .views import index,save_status,profile,book_table,get_Json,email_checking,subscribe,display_subscribers,subscribers_table,delete_subscriber,my_view,login,logins,logout
from .views import *

urlpatterns = [
   path('',index, name='index'),
   #path('',status,name='status'),
   path('save_status/',save_status,name='save_status'),
   path('profile/',profile,name='profile'),
   path('story9/',book_table,name='story9'),
   path('getjson/<str:keyword>',get_Json,name='getjson'),
   path('emailcheck/',email_checking,name='emailcheck'),
   path('subscribe/',subscribe,name='subscribe'),
   path('display_subscribers/',display_subscribers,name='display_subscriber'),
   path('challenge10/',subscribers_table,name='challenge10'),
   path('deleted/',delete_subscriber,name='deleted'),
#    path('coba2login/',logins,name='coba2login'),
#    path('logins/',my_view,name='login'),
   path('login/',login,name='login'),
   path('logout/',logout,name='logout'),
   path('flashcard/',flashcard,name='flashcard'),
   path('gettjson/',get_json,name='get_json')
#    path('appippw/mydiary/new/',post_diary,name='postdiary'),
#    path('apippw/mydiary/list/',get_diary,name='getdiary')


]
